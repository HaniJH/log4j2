package com.gilmard.logging.log4j2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.parser.ParseException;

public class SampleLogger {

//    private static final Logger LOGGER = LogManager.getRootLogger();
    private static final Logger LOGGER = LogManager.getLogger(SampleLogger.class);
//    private static final Logger LOGGER = LogManager.getLogger("Console");
//private static final Logger SOCKET_LOGGER = LogManager.getLogger("socketLogger");

    public static void main(String[] args) throws ParseException {

        System.out.println("App started...");

        // log something
        LOGGER.debug("1 - this is a log.");
        LOGGER.debug("2 - this is a log.");
        LOGGER.debug("3 - this is a log.");
        LOGGER.debug("4 - this is a log.");
        LOGGER.debug("5 - this is a log.");
        LOGGER.info("6 - this is a log.");
        LOGGER.info("7 - this is a log.");
        LOGGER.info("8 - this is a log.");
        LOGGER.info("9 - this is a log.");
        LOGGER.info("10 - this is a log.");
        LOGGER.debug("this one is for socket logger");

        System.out.println("... App finished.");
    }
}
